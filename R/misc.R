#'@import shiny
#'@import shinydashboard
#'@import rhandsontable
#'@import shinyBS2
#'@importFrom magrittr '%>%'
#'@export
magrittr::`%>%`

.onAttach <- function(...) {

  # Create link to javascript and css files for package
  shiny::addResourcePath("mmgeCatalogs", system.file("www", package="mmgeCatalogs"))

}

mmgeCatalogsDep <- htmltools::htmlDependency(
  "mmgeCatalogs",
  packageVersion("mmgeCatalogs"),
  src = c(href = "mmgeCatalogs"),
  stylesheet = c(
    "mmgeCatalogs.css",
    "bootstrap-tour.css"
  ),
  script = c(
    "bootstrap-tour.js",
    "tour.js",
    "mmgeCatalogs2.js"
  )
)

#'@export
Mode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

check_decimals <- function(x) {

  unname(ceiling(quantile(sapply(strsplit(as.character(x), ".", fixed = TRUE), function(w) {
    if(length(w) == 2) {
      nchar(w[2])
    } else {
      0
    }
  }), 0.90)))

}