#'@export
publish_catalog_data <- function(catalog) {

  test_catalog(catalog)

  if(!"catalog" %in% names(catalog)) {
    catalog <- build_catalog(from_mongo = FALSE)
  }

  m <- mongolite::mongo(
    collection = catalog$config$name,
    db = catalog$config$protocol,
    url = "mongodb://ldap.medgen.iupui.edu",
    verbose = TRUE
  )

  data <- catalog$catalog
  data$last_update <- Sys.Date()
  catalog$last_update <- Sys.Date()

  try(m$drop(), silent = TRUE)

  m$insert(data)

  return(invisible(TRUE))

}