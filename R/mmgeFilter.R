# These functions are the custom filter that is used by DataTables to do server
# side processing of the catalog files. mmgeFilter is the primary function,
# the others are helper functions for the specific filter types.
selectFilter <- function(value, data, remove_missing) {

  isna <- is.na(data)
  if(all(value == "")) {
    ismatch = rep(TRUE, length(data))
  } else {
    # Remove HTML tags from the data if they are present
    if(any(grepl("<.*?>", data))) {
      data <- gsub("<.*?>", "", data)
    }
    ismatch <- data %in% value
  }
  if(remove_missing) {
    return(ismatch & !isna)
  } else {
    return(isna | ismatch)
  }

}

textFilter <- function(value, data, remove_missing) {
  print(value)
  isna = is.na(data)
  if(value == "") {
    ismatch = rep(TRUE, length(data))
  } else {
    ismatch = grepl(value, data)
  }
  if(remove_missing) {
    return(ismatch & !isna)
  } else {
    return(isna | ismatch)
  }

}

numericFilter <- function(value, data, remove_missing) {

  isna <- is.na(data)
  val <- as.numeric(value)
  ismatch <- val[1] <= data & data <= val[2]
  if(remove_missing) {
    ismatch[is.na(ismatch)] <- FALSE
  } else {
    ismatch[is.na(ismatch)] <- TRUE
  }

  return(ismatch)

}

dateFilter <- function(value, data, remove_missing) {

  isna <- is.na(data)
  val <- as.Date(value)
  if(all(value == "")) {
    ismatch = rep(TRUE, length(data))
  } else {
    ismatch = val[1] <= data & data <= val[2]
  }
  if(remove_missing) {
    ismatch[is.na(ismatch)] <- FALSE
  } else {
    ismatch[is.na(ismatch)] <- TRUE
  }

  return(ismatch)

}

mmgeFilter <- function(data, params) {

  filters <- list(
    slider = numericFilter,
    date = dateFilter,
    text = textFilter,
    select = selectFilter
  )

  n <- nrow(data)

  keep <- rep(TRUE, nrow(data))

  for(i in seq(ncol(data))) {
    search_column <- params$columns[[i]]
    if(search_column$search$value != "") {
      search <- jsonlite::fromJSON(search_column$search$value)
      if(is.na(search[['type']])) {
        search$type = "text"
        search$filter = ""
      }
      if(is.null(search[['filter']])) {
        search$filter = ""
      }
      if(is.na(search[['filter_missing']])) {
        search$filter_missing <- FALSE
      }
      filter <- filters[[search$type]]
      keep <- keep & filter(search[['filter']], data[[search_column$name]], search[['filter_missing']])
    }
  }

  fdata <- data[keep, ]

  if(!is.null(params$order)) {
    for(s in rev(params$order)) {
      fdata <- fdata[order(fdata[, as.integer(s$column) + 1], decreasing = (s$dir == "desc"), method = "radix"), ]
    }
  }

  s <- as.integer(params$start)
  l <- min(as.integer(params$length), (nrow(fdata) - s))

  fdata <- fdata[(s + 1):(s + l), ]

  return(list(
    draw = as.integer(params$draw),
    recordsTotal = n,
    recordsFiltered = sum(keep),
    data = unname(fdata),
    DT_rows_all = which(keep),
    DT_rows_current = which(rownames(data) %in% rownames(fdata))
  ))

}
