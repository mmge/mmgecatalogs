# Ordering Samples

Ordering samples with the catalog can be done in a couple of ways. You can use 
the filters to filter down to a subset of records and request all those samples
or you can select specific specimens by clicking to highlight those specimens. 
You can then request only those specimens you have selected.

## Selecting Specimens

If you want more control than filtering specimens provides you will need to select
the specific specimens you wish to request. Please see the following section for
help with selecting specimens in the catalog.

### Selecting Individual Specimens

To select an individual specimen you can simply click on the record for that 
specimen. The specimen will gain a blue highlight.

![Sample Selection](mmgeCatalogs/images/sample_selection.png)

In the example above sample PH0000-0005-1 has been selected. To unselect a sample, 
simply click the sample record again. The blue highlight will disappear to indicate
that the sample is no longer selected. In addition to the highlighting, the footer
also keeps a running total of how many samples you have selected.

To select multiple specimen at once you can hold `Shift` while clicking sample
records. Doing so will cause all samples between the two records to be selected.

### Selection Tools

The "Selection" button in the tool bar provides a few convenience utilities to 
aid in sample selection. From this menu you may select all filtered specimens,
unselect all specimens, or invert the selection of currently filtered specimens.

* Select All - Select all specimens in the catalog currently visible
* Select None - Deselect all specimens in the catalog
* Invert Selection - Select all currently deselected specimens, deselect all currently selected specimens

## Placing an Order

Placing an order really just involves downloading a record of the specimens you 
want to request. The "Download" button provides three options for downloading your
request.

### Download All

Selecting "Download All" will download the entire catalog, including any new data
you may have joined to the catalog. Use this option if you want to use an outside
application to further process your order. 

### Download Filtered

Selecting "Download Filtered" will download the catalog as it appears after all
current filters are applied. If no filters have been applied this would be the same
as "Download All". 

### Download Selected

The "Download Selected" option is only available if you have actually selected at
least one sample. Selecting this option will download a CSV file with only those
records that you specifically selected.

