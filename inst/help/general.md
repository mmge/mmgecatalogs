# IUGB Catalog System

Welcome to the IUGB Biospecimen Cataloging System. Use this catalog to identify available biospecimens. You may use the tabs at the top of this window to
read more detailed help about specific sections of the catalog or click the 
button in the toolbar labeled 'Tour':

![Tour Button](mmgeCatalogs/images/tour_button.png)

Clicking the button will open the catalog tour:

![Tour Popup](mmgeCatalogs/images/tour.png)

Click "Next" to step through explanations of all the functionality of the catalog.
You may click "End Tour" at any time to end the tour and start using the catalog.

## Catalog Sections

There are four main sections of the application: the toolbar, the side bar, the
catalog, and the footer. Each section is explained below.

### The Toolbar

![The Toolbar](mmgeCatalogs/images/toolbar.png)

The toolbar contains several buttons that allow you to interact with the application.

* **Dictionary** - Open a data dictionary with explanations of the catalog fields
* **Join Data** - Opens a dialog that allows you to join external data to the 
  catalog. See the section on "Joining" for more information.
* **Selection** - Offers additional tools for selecting specimens from the catalog.
* **Download** - Allows you to download catalog data
* **Help** - Opens this Help window
* **Tour** - Opens the Catalog Tour

The far right of the toolbar also shows who is currently logged in and offers a
button for logging out when you are finished.

### The Sidebar

![The Sidebar](mmgeCatalogs/images/sidebar.png)

The sidebar houses the controls for viewing and filtering catalog fields. Each
available field in the catalog has a corresponding control in the sidebar. Use 
the checkbox on the left to turn visibility of the field on or off. Click the 
<i class = "fa fa-filter"></i> button on the right to view filtering options for
the field. See the section on "Filtering" for more detailed help.

### The Catalog

![The Catalog](mmgeCatalogs/images/catalog.png)

The catalog is where biospecimen and any joined clinical data is displayed. Use the controls in the sidebar to turn columns on or off and filter the catalog based on field values. You may also sort the catalog by clicking on column headers. Holding `Shift` while clicking column headers allow you to sort based on multiple columns.

### The Footer

![The Footer](mmgeCatalogs/images/footer.png)

The footer displays record count and selection information about the current state of the catalog based on any applied filters. It lets you know the total number of records in the catalog, how many are visible based on current filters, and how many samples have been selected.

