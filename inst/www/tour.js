var tour = new Tour({
  steps: [
    {
      title: "Welcome",
      content: "Please take a short tour to familiarize yourself with the application",
      orphan: true
    },
    {
      element: "#dictionaryButton",
      title: "Data Dictionary",
      content: "Click this button to open the data dictionary. The data dictionary provides more information about the data in the catalog.",
      placement: "bottom"
    },
    {
      element: "#joinButton",
      title: "Joining External Data",
      content: "Click this button to open the data joining dialog. Use it to bring data in from external sources. See the help page on joining for more details.",
      placement: "bottom",
      onNext: function(tour) {$("#joinModal").modal('show');}
    },
    {
      element: "#joinModal .btn-file",
      title: "Upload Data",
      content: "Click the 'Browse' button to open a file dialog. Use that file dialog to select files to join to the catalog.",
      position: "right",
      delay: 500,
      onHide: function(tour) {$("#joinModal").modal('hide');}
    },
    {
      element: "#selectionButton",
      title: "Selecting Specimens",
      content: "Select specimens by clicking on the row in the table you want. ",
      placement: "bottom",
      onShown: function(tour) {
        $("#selectionButton")
          .addClass('open')
          .on("hide.bs.dropdown.tour", function() {return false;});
      }
    },
    {
      element: "#selectionButton > .dropdown-menu",
      title: "Selecting Specimens",
      content: "Use the buttons in this dropdown to Select All, Select None, or invert your selection.",
      onHide: function(tour) {
        $("#selectionButton")
          .removeClass('open')
          .off("hide.bs.dropdown.tour");
      }
    },
    {
      element: "#downloadButton",
      title: "Download Request",
      content: "Once you've completed joining, filtering, and selecting you may download your request as a csv file.",
      placement: "bottom",
      onPrev: function(tour) {tour.goTo(tour.getStep() - 2)},
      onShown: function(tour) {
        $("#downloadButton")
          .addClass('open')
          .on("hide.bs.dropdown.tour", function() {return false;});
      }
    },
    {
      element: "#downloadButton > .dropdown-menu",
      title: "Download Request",
      content: "This dropdown gives you the option to download the entire catalog, the results of your filtering, or just the specimens you've selected.",
      onHide: function(tour) {
        $("#downloadButton")
          .removeClass('open')
          .off("hide.bs.dropdown.tour");
      }
    },
    {
      element: "#helpButton",
      title: "More Help",
      content: "If you need more help try clicking here to access the more detailed help pages.",
      placement: "bottom",
      onPrev: function(tour) {tour.goTo(tour.getStep() - 2)},
    },
    {
      element: "#tourButton",
      title: "Take a tour",
      content: "This button opens the tour you are currently viewing.",
      placement: "bottom"
    },
    {
      element: "#columnControlFilterSearch",
      title: "Filter Sidebar",
      content: "If you've joined many columns you may use this text input to filter down to find the exact column you are interested in.",
      placement: "bottom"
    },
    {
      element: "#columnControlFilter",
      title: "Filter Sidebar",
      content: "The sidebar has been filtered down to just those columns with an 's' in the column name.",
      onShow: function(tour) {$("#columnControlFilterSearch").val("s").trigger("keyup");}
    },
    {
      element: "#columnControlFilterReset",
      title: "Resetting the sidebar",
      content: "To restore the full list of available columns you can click this button.",
      onHide: function(tour) {$("#columnControlFilterReset").trigger("click");}
    },
    {
      element: ".catalog-control:first-child > .column-display input[type='checkbox']",
      title: "Toggle field visibility",
      content: "Use these checkboxes to toggle the visibility of columns in the catalog"
    },
    {
      element: ".catalog-control:first-child > .column-display .filter-toggle",
      title: "Toggle filter visibility",
      content: "Use the filter button to show/hide the filter tool for the column.",
      onShown: function(tour) {$(".catalog-control:first-child > .column-display .filter-toggle").trigger("click")}
    },
    {
      element: ".catalog-control:first-child .column-filter .form-group:first-child",
      title: "Column Filter",
      content: "Use column filters to filter catalog records down to those you are interested in.",
    },
    {
      element: ".catalog-control:first-child .column-filter .form-group:last-child",
      title: "Remove Missing Values",
      content: "If you want to exclude missing values in the column you must check this box."
    },
    {
      element: ".catalog-control:first-child > .column-display .filter-reset",
      title: "Reset Filter",
      content: "Use this button to reset the column filter back to defaults.",
      onHidden: function(tour) {$(".catalog-control:first-child > .column-display .filter-toggle").trigger("click")}
    },
    {
      element: ".catalog-control:first-child > .column-display .dismiss-column",
      title: "Dismiss Column",
      content: "Use this button to completely remove a column from the catalog and the sidebar."
    },
    {
      title: "Thanks",
      content: "When you are done, you can logout using the button in the upper-right, or simply close the browser.",
      orphan: true
    }
  ]
});

$(document).ready(function() {

  tour.init();

  $("#tourButton").on("click", function(e) {
    if(tour.ended()) {
      tour.restart();
    }
    tour.start(true);
    tour.goTo(0);
  });
});